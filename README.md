superagent-jsonparse
--------------------

Provide a streaming parser for [superagent](https://www.npmjs.com/package/superagent) based on jsonparse.

Installation
------------

```shell
npm install superagent-jsonparse
```

```shell
yarn add superagent-jsonparse
```

Usage
-----

```javascript
const request = require('superagent');
const jsonParser = require('superagent-jsonparse');
const Decimal = require('decimal.js-light');

{body} = await request
    .get('https://example.com/data.json')
    .accept('json')
    .use(jsonParser( (text) => new Decimal(text) ))
// body contains arbitrary-precision Decimal for all JSON numbers
```

Description
-----------

This package is a superagent plugin which replaces the default JSON parser (`JSON.parse`) with a streaming parser.

Optionally, you may provide a reviver for parsed numbers, which allows to implement arbitray-precision (big-number, big-decimal) parsing of JSON numbers. The default reviver for numbers is `(text) => new Number(text)`, which provides functionality similar to `JSON.parse`.

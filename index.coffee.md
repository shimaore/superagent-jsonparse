    Parser = require '@shimaore/jsonparse'

    exact = (numberReviver) -> (p) ->
      p
      .buffer(false)
      .parse (res,cb) ->
        parser = new Parser
        if numberReviver?
          parser.numberReviver = (text) ->
            try
              result = numberReviver text
            catch error
              return error
            this.onToken Parser.C.NUMBER, result
            return
        res.body = null
        parser.onError = (err) ->
          cb err, res
          return
        parser.onValue = (value) ->
          if this.stack.length is 0
            cb null, value
          return
        res.on 'data', (chunk) ->
          parser.write chunk
          return
        res.once 'end', ->
          return
        return

    module.exports = exact

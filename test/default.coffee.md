    test = require 'ava'
    request = require 'superagent'
    http = require 'http'
    Decimal = require 'decimal.js-light'
    jsonParser = require '..'

    server = null
    test.before ->
      server = http.createServer (req,res) ->
        res.statusCode = 200
        res.setHeader 'Content-Type', 'application/json'
        res.end '{"value":59}'
      server.listen 3000, '127.0.0.1'
      return
    test.after ->
      server.close()
      return

    test 'it should parse numbers properly', (t) ->
      {body} = await request
        .get 'http://127.0.0.1:3000/'
        .accept 'json'
        .use jsonParser()
      t.is body.value, 59
      return

    test 'it should parse numbers as string', (t) ->
      {body} = await request
        .get 'http://127.0.0.1:3000/'
        .accept 'json'
        .use jsonParser (text) -> text
      t.is body.value, '59'
      return

    test 'it should parse numbers as exact decimal', (t) ->
      {body} = await request
        .get 'http://127.0.0.1:3000/'
        .accept 'json'
        .use jsonParser (text) -> new Decimal text
      t.true body.value.equals new Decimal 59
      return
